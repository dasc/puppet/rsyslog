class rsyslog (
    String $owner,
    String $group,
    String $filemode,
    String $dirmode,
    String $umask,
    String $template,
    String $local_file_store,
    String $loghost,
    Integer $loghost_port,
    Enum['tcp', 'udp', 'relp'] $loghost_protocol,
    Integer $collector_port,
    Array[String] $collector_address,
    Enum['tcp', 'udp', 'relp'] $collector_protocol,
    Optional[String] $collector_logdir = undef,
    Array[String] $extra_config,
) {
    package {'rsyslog': ensure => installed, }
    if $loghost_protocol == 'relp' {
        package {'rsyslog-relp': ensure => installed, }
    }

    file {'/etc/rsyslog.conf':
        ensure => file,
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp($template, {
            owner => $owner,
            group => $group,
            filemode => $filemode,
            dirmode => $dirmode,
            umask => $umask,
        }),
        notify => Service['rsyslog'],
    }

    if $collector_logdir {
        file {'rsyslog_collector_logdir':
            path => "${collector_logdir}",
            ensure => directory,
            owner => 'root',
            group => 'root',
            mode => '0755',
            notify => Service['rsyslog'],
        }
        file {'/etc/rsyslog.d/60-centralcollector.conf':
            ensure => 'file',
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => epp('rsyslog/60-collector.conf.epp', {
                collector_logdir => $collector_logdir,
                collector_address => $collector_address,
                collector_port => $collector_port,
                collector_protocol => $collector_protocol,
            }),
            notify => Service['rsyslog'],
        }
    }

    if $loghost {
        file {'/etc/rsyslog.d/50-forwarder.conf':
            ensure => 'file',
            owner => 'root',
            group => 'root',
            mode => '0644',
            content => epp('rsyslog/50-forwarder.conf.epp', {
                loghost => $loghost,
                loghost_protocol => $loghost_protocol,
                loghost_port => $loghost_port,
            }),
            notify => Service['rsyslog'],
        }
    }

    $extra_config.each |$config| {
        file {"/etc/rsyslog.d/$config":
            ensure => file,
            owner => 'root',
            group => 'root',
            mode => '0644',
            source => ["${local_file_store}/etc/rsyslog.d/${config}", ],
            notify => Service['rsyslog'],
        }
    }

    service {'rsyslog':
        ensure => 'running',
        enable => true,
    }
}
